# Change this to the version you use :)
PREFIX=/usr/lib/python2.7
BINPATH=/usr/bin
DOCPATH=/usr/doc
MANPATH=/usr/share/man
export MANPATH
all:
	@echo "Please specify operation"
	@echo "========================"
	@echo "Available operations:"
	@echo ""
	@echo "Note: for the install, update, and uninstall rules, you can pre-append \"docs-\" or \"base-\" to limit operations to either docs or libraries, respectively."
	@echo
	@echo "install    - Install library and all man pages normally."
	@echo "update     - Update current installation, calling uninstall and install in sequence."
	@echo "uninstall  - Uninstall the library and its man pages."
	@echo "clean      - remove all *.pyc files and any extraneous build resources."
	@echo "strip      - Strip all git info and building tools."
	@echo "release    - Generate a directory suitable for sharing with others."
	@echo "========================"
	@echo "Remember, for all but the clean and strip commands, you must use sudo to install to system directories."
	@echo ""
	@echo "Reminder: To build the manpages, you need to install ronn (See INSTALL.md)"


# Utility rules
genWorkSpace:
	@echo "[Generating workspace...]"
	mkdir ./wst
	cp -r net wst
	cp -r sec wst
	cp -r trm wst
	cp -r utl wst
	cp -r fio wst
	cp -r ext wst

	cp __init__.py wst
	cp README.md wst
	cp LICENSE wst
	cp contrib.md wst

	@echo "[Generation complete.]"

genDocs:
#	This is to prevent stupid from happening at any point lol
	@echo "[Clearning manpage cache...]"
	$(MAKE) -C doc/manpages/ clean

	@echo "[Building manpages...]"
	$(MAKE) -C doc/manpages/
	@echo "[Generation complete]"

strip:
	rm -rf .git
	rm .gitignore
	rm makefile
	rm build.bat
	rm doc/manpages/makefile
	rm doc/manpages/.gitignore

release: clean genWorkSpace genDocs
	mkdir ./wst/man
	mkdir ./wst/man/man3
	mkdir ./wst/man/man5
	cp doc/manpages/build/*.3.gz ./wst/man/man3/.
	cp doc/manpages/build/*.5.gz ./wst/man/man5/.


# Cleaning Rules
cleanDocs:
	@echo "[Purging built manpages...]"
	@$(MAKE) -C doc/manpages/ clean

clean: cleanDocs
	@echo "[Purging *.pyc files...]"
	find . -name "*.pyc" -delete
	@echo "[Purging old builds(if exist)...]"
	rm -rf wst
	@echo "[Cleanup complete!]"


# Install Rules
install: base-install docs-install

docs-install: genDocs
	mv doc/manpages/build/*.3.gz $(MANPATH)/man3/.
	mv doc/manpages/build/*.5.gz $(MANPATH)/man5/.

base-install: clean genWorkSpace
	cp -r wst $(DESTDIR)$(PREFIX)/.
	rm -r wst


# Uninstall Rules
uninstall: base-uninstall docs-uninstall

base-uninstall:
	rm -r $(DESTDIR)$(PREFIX)/wst

docs-uninstall:
	@$(MAKE) -C doc/manpages/ uninstall


# Update Rules
update: uninstall install

base-update: base-uninstall base-install

docs-update: docs-uninstall docs-install
