set pypath=C:\Python27\Lib\
REM Basic prototype build script for yes reasons of yesitude(tm)
@echo off
echo "[Creating directory structure...]"
@echo on
del wst
mkdir wst
xcopy /s net wst
xcopy /s sec wst
xcopy /s trm wst
xcopy /s utl wst
xcopy /s fio wst
xcopy /s ext wst

xcopy __init__.py wst
xcopy README.md wst
xcopy LICENSE wst
xcopy contrib.md wst

@echo off
echo "[Complete. Copying into C:\Python27\Lib\...]"
@echo on

xcopy /s wst %pypath%
