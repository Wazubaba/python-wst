import random

# translated from the weird one-liner this guy gave:
# http://www.evanfosmark.com/2008/06/xor-encryption-with-python/

def genkey(entropy=32):
	"""
		Generates a random string of numbers and letters
		with a size of <entropy>.

		Takes:
			int		-	size of return string
		Returns:
			string	-	random letters and numbers
	"""
	letters = []
	numbers = []
	key = ""
	for i in range(97,123):
		letters.append(chr(i))
	for i in range(65,91):
		letters.append(chr(i))
	for i in range(0,10):
		numbers.append(str(i))

	for i in range(entropy):
		if random.choice([1,2]) == 1:
			key += random.choice(letters)
		else:
			key += random.choice(numbers)
	
	return key

def crypt(data, key=""):
	"""
		Encrypts a string via XOR encryption. To decrypt, simply
		run the process on the encrypted string with the same
		key used for the encryption.

		Takes:
			string	-	data to encrypt
			key		-	a random key to use, must be same size as data!
		Returns:
			tuple	-	encrypted string, key used to encrypt
	"""
	if not key:
		random.seed()
		key += genkey(len(data))
		print("generated key: %s" %key)
	
	arc = ""
	for i in range(len(data)):
		arc += chr(ord(data[i]) ^ ord(key[i]))

	return (arc,key)

if __name__ == "__main__":
	from wst.trm import log
	log.logger = "Crypt"
	log.notice("Testing module...")
	log.log("Encrypting 'fish'...")

	comp = crypt("fish")
	log.log(comp)
	comp = crypt(comp[0],comp[1])
	log.log(comp)

