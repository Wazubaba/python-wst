import hmac
from wst.trm import log

DEBUG = False
NOTICES = False

"""
	Options:
		boolean		-	DEBUG	: enable debug info
		boolean		-	NOTICES : enable stdout notifications
"""

def hash(line, pubkey):
	"""
		Generates an md5 hash of a given <line> using
		<pubkey>.

		Takes:
			string	-	line to crypt
			string	-	key to use for hash generation
		Returns:
			string	-	hashed version of line
	"""
	digest_maker = hmac.new(pubkey)
	digest_maker.update(line)
	return digest_maker.hexdigest()

def checksum(fh, pubkey):
	"""
		Generates a checksum for a file at path fh.
		uses pubkey to generate the md5 hash.
	
		Takes:
			string	-	file name
			string	-	key to use for hash generation
		Returns:
			string	-	md5 checksum as string
	"""
	digest_maker = hmac.new(pubkey)
	with open(fh, "rb") as f:
		while True:
			block = f.read(1024)
			if not block: break
			digest_maker.update(block)
	digest = digest_maker.hexdigest()
	return digest

def validate(fh, testhash, pubkey):
	"""
		Validates a file at path fh.
		uses pubkey to generate an md5 key to test against testhash.

		Takes:
			string	-	file name
			string	-	md5 hash as string
			string	-	key to use for hash generation
		Returns:
			boolean -	If verification is successful returns True,
						else False
	"""
	digest = checksum(fh, pubkey)

	if DEBUG:
		print("File:\t%s" %digest)
		print("Test:\t%s" %testhash)

	if digest == testhash:
		if NOTICES:
			log.logger = "Validator"
			log.good("Verification successful.")
		return True
	else:
		if NOTICES:
			log.logger = "Validator"
			log.bad("Verification failed.")
		return False

if __name__ == "__main__":
	fh = "./verifyer.py"
	pub = "AUGH"
	NOTICES = True
	
	cs = checksum(fh, pub)
	if validate(fh, cs, pub):
		log.good("YEAS AUGH AUGH AUGH")
	else:
		log.bad("NOA UAG(WEHTG(AQH")

