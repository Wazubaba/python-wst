from sys import argv
class ArgDatabase(object):
	def __init__(self):
		self.args = {}
		self.argString = ""

	def test(self, arg = ""):
		"""
			Synposis: Helper function for getting a key's value if it exists

			Takes: string arg - argument to look up

			Returns: Either string or bool value

			Notes: If no hit is found, this function will return bool False
		"""
		if arg in self.args:
			return self.args[arg]
		else: return False

	def parse(self):
		"""
			Synposis: Parse arguments into an easy to read dict

			Takes: None

			Returns: Dict args - dictionary containing arguments
			as keys with arg values as the values.

			Notes: Currently supports single letter arguments,
			word arguments, and atom arguments, as in if you
			supply an arg but no value, it will just be set
			as bool True in the dict :P
		"""

		largs = {}
		wargs = {}
		flag = ""
		arg = ""
		state = 0
		warg = 0

		# Step 1: Convert into a string for simpler parsing
		for temp in argv[1:]:
			self.argString += temp+" "


		#Step 2: Actually parse the bloody thing
		for index in range(len(self.argString)):
			char = self.argString[index]

			if state == 0:
				if char == "-": state = 1

			elif state ==  1: #test if it's a letter flag or a word flag
				if char == "-": state = 10
				else:
					flag = char
					state = 2

			elif state == 2: #skip till we get to the arg
				if char == "-": # Allow for a flag to be an atom
					self.args[flag] = True
					flag = ""
					arg = ""
					state = 1
				elif char != " ":
					arg += char
					state = 3
				
			elif state == 3: #read in the arg and append to the argsdict
				if char == "-":
					self.args[flag] = True
					flag = ""
					arg = ""
					state = 1
				if char == " ":
					if warg == 0: wargs[flag]=arg
					else: largs[flag]=arg

					self.args[flag]=arg
					#reset vars
					flag = ""
					arg = ""
					state = 0
				else:
					if char != "-":
						arg += char

			elif state == 10: #handle word flags
				if char == " ":
					state = 3 #get the arg and append
					warg = 1
				else: flag += char
		if flag != "": # Make it so the last var can be an atom
			self.args[flag] = True

#--#
#TH#
#--#
if __name__ == "__main__":
	from wst.trm import log
	log.FORMAT = "*m"
	log.out(" ################################")
	log.out("###TEST HARNESS FOR ARGPARSE.PY###")
	log.out(" ################################")
	if len(argv) < 2:
		log.error("Please include an arg or two for testing :P")
		from sys import exit
		exit(-1)

	argdb = ArgDatabase()
	argdb.parse()
	
	log.out("\nUsing [[%s]] as the arg string" %(argdb.argString))

	for key in argdb.args:
		log.out("%s -> %s" %(key,argdb.args[key]))
	log.out("\n***DICT***")
	log.out(str(argdb.args))
