from wst.trm import colors
import datetime
# Global Variables
# General Vars
LOGGER  = ""   # Name of the thing calling for logging.
FORMAT  = ""   # Format to use globally.
STDOUT  = True # Write to stdout.

# Log vars
LOGFILE = ""   # If left blank, do not write to a log file.
LOGFORMAT = "" # Overrides the formatting to display differently in the log files

__doc__="""
	Log module

	There are several global variables that allow for tweaking.

	LOGGER is meant to be the name of the calling function or entity.

	FORMAT is the global format for output to stdout.

	STDOUT controls whether stdout gets printed to.
	
	LOGFILE is the name of a log file to write to, if left blank no
	log file is written to.

	LOGFORMAT is the global format for output to logfiles. You can
	safely just set this to FORMAT; All colors will be culled.

	There are two functions defined;
	 * fmtParse() - Meant to be used internally and by libraries
					that want to parse format strings.
	 * out() - Outputs to the log.

	FORMAT Specfication
	===================

	The following chars when placed after an asterisk(*) will
	be replaced with the matching data:

		l	-	logger
		m	-	message
		d	-	date
		t	-	time

	As an example, the preset format is `[*l:*m]`.

	Basic colors are also supported:

		G	-	Green
		R	-	Red
		B	-	Blue
		X	-	Black
		M	-	Magenta
		W	-	White
		C	-	Cyan
		Y	-	Yellow
		0	-	Reset colors to terminal default

	You don't need to really worry about terminating your colors
	with *0, as the parser will automatically ensure your format
	string gets terminated with it if needed.
"""

def fmtParse(msg = "", format = "", enableColors = STDOUT):
	"""
		Synopsis:
			Formats a string to have color and additional info.
		Notes:
			Don't bother calling this unless you are using it in your own
			logging system.
		Takes:
			string  msg          -  Message to include
			string  format       -  Format string to decorate msg with
			boolean enableColors -  Controls whether colors will be allowed or not
		Returns:
			string  retmsg   -  Formated string for printing
	"""
	now = datetime.datetime.now()

	if enableColors:
		mapping = {
			"l":LOGGER,
			"m":msg,
			"d":now.strftime("%Y-%m-%d"),
			"t":now.strftime("%H:%M"),
			"G":colors.GREEN,
			"R":colors.RED,
			"B":colors.BLUE,
			"X":colors.BLACK,
			"M":colors.MAGENTA,
			"W":colors.WHITE,
			"C":colors.CYAN,
			"Y":colors.YELLOW,
			"0":colors.DEFAULT,
			"*":"*",	# replace escaped '*' with '*'
		}
	else:
		mapping = {
			"l":LOGGER,
			"m":msg,
			"d":now.strftime("%Y-%m-%d"),
			"t":now.strftime("%H:%M"),
			"G":"",
			"R":"",
			"B":"",
			"X":"",
			"M":"",
			"W":"",
			"C":"",
			"Y":"",
			"0":"",
			"*":"*",
		}

	retmsg = ""
	tog = False
	if len(format) < 2: format += "*0"
	if format[3:] != "*0": format += "*0" # Ensure the format string is always terminated
	for char in format:
		if not tog:
			if char == '*':
				tog = True
				continue

		if tog:
			if char in mapping:
				retmsg += mapping[char]
			else:
				if not enableColors:
					retmsg += '*' # replace missing '*'
					retmsg += char

			tog = False
		else:
			retmsg += char


	return retmsg

def out(msg = "", override = FORMAT, logfileOverride = LOGFORMAT, enableColors = STDOUT):
	"""
		Synopsis:
			Outputs a message to the predefined locations.
		Takes:
			string  msg             -  Message to print
			string  override        -  Format to override the global format setting with
			string  logfileOverride -  Format to override global logfile format setting with
			boolean enableColors    -  Toggle whether colors are included in output
		Returns:
			None
	"""

	output = fmtParse(msg, override, enableColors)

	if STDOUT: print(output)

	if LOGFILE != "":
		output = fmtParse(msg, logfileOverride, False)
		with open(LOGFILE, "a") as fp:
			fp.write(output+"\n")

## Test Harness ##
if __name__ == "__main__":
	def cat(fname):
		with open(fname, "r") as fp: print(fp.read())

	print("Testing output")
	out("test", "*W[*Grawr*W]{*R*d*W|*M*t*W}:*C***m***0")

	print("\nTesting stdout disabling")
	STDOUT = False
	out("nya", "*W*m")

	print("\nTesting logfile creation")
	LOGFILE = "testfile.log"
	out("test", logfileOverride = "ohaider: *m")
	print("***contents of testfile.log***")
	cat("testfile.log")

	print("\nTesting stdout output and logfile output. LOGGER set to nya")
	STDOUT = True
	LOGGER = "nya"
	out("ohaider u", "*W[*G*l*W]{*R*d*W|*M*t*W}:*C***m***0", "*W[*G*l*W]{*R*d*W|*M*t*W}:*C***m***0")
	print("***contents of testfile.log***")
	cat("testfile.log")

	print("\n\nTest complete!")

