from wst.trm import colors
import datetime
# NOTE: not 100% certain how well this works on windows, I know windows
# uses a different method for their color escapes in terminals... if they
# even HAVE them...
# Some day I'll try to windows support this thing...

print("log module 1 is depricated, you should really be using log2!")
print("As long as you only used out(), no change is really needed!")

__doc__="""
	Log module
	A collection of simple binds to help with output formatting.

	<LOGGER> should be set before any serious usage, make it
	something meaningful to operation, such as a subsystem, or
	the name of the program itself.

	While you can use the simple macro functions for basic color
	support, by manually redefining <FORMAT>, you can get really
	intricate, with access to a hopefully simple mini formatting
	language.

	To toggle whether a log file should also be written to during
	operation, you can set <WRITE> to True or False. The file will
	default to being named CHANGEME.log, so remember to manually
	set the <LOGFILE> string. The old file will automatically be
	closed while the new one will be opened.

	Ensure you also disable <WRITE> before ending your program, as
	otherwise it cannot be guarenteed that your log will be properly
	written.

	You can configure the colors by editing the colors that
	the different macros [GOOD,WARN,ERRO,CRIT,NOTI] point to.
	Simply point them to a function that formats the way you wish.

	By default, the macros are:
		GOOD	-	colors.green
		WARN	-	colors.yellow
		ERRO	-	colors.red
		CRIT	-	colors.bred
		NOTI	-	colors.magenta


	FORMAT Specfication
	===================

	The following chars when placed after an asterisk(*) will
	be replaced with the matching data:

		l	-	logger
		m	-	message
		d	-	date
		t	-	time

	As an example, the preset format is `[*l:*m]`.

	Basic colors are also supported:

		G	-	Green
		R	-	Red
		B	-	Blue
		X	-	Black
		M	-	Magenta
		W	-	White
		C	-	Cyan
		Y	-	Yellow
		0	-	Reset colors to terminal default

	Please note:
		Multiple colors in a log message is very fun and
	great, but the vast majority of this module
	automatically assigns colors, so if you intend to do
	anything really nifty, you should probably stick to
	manually calling `out(msg)` so as to not have odd
	effects :D
"""

LOGGER = "CHANGEME"
FORMAT = "[*l:*m]"
LOGFILE = "CHANGEME.log"
COLORBLIND = False
WRITE  = False

GOOD = "*G"
WARN = "*Y"
ERRO = "*R"
CRIT = "*W"
NOTI = "*M"

logfp = 0
OldLogFile = "CHANGEME.log"


isFirstStart = True #This is for the out function to set <OldLogFile>

def genMsg(msg,overide="",colorTog=False):
	"""
		Synopsis:
			This is what parses and tokenizes the FORMAT string
			into the proper output, and returns it.
		Notes:
			You don't need to call this yourself, unless you
			want the exact log string for something.
		Takes:
			string msg      -	string to use as message
			string overide  -	format string to override FORMAT
			bool   colorTog -	if False, disable colors and eat
			                    color flags(helper for write function)
		Returns:
			string retmsg	-	Fully constructed message as string
	"""

	if overide == "": overide = FORMAT # allow FORMAT to be overrided
	now = datetime.datetime.now()

	if not colorTog:
		mapping = {
			"l":LOGGER,
			"m":msg,
			"d":now.strftime("%Y-%m-%d"),
			"t":now.strftime("%H:%M"),
			"G":colors.GREEN,
			"R":colors.RED,
			"B":colors.BLUE,
			"X":colors.BLACK,
			"M":colors.MAGENTA,
			"W":colors.WHITE,
			"C":colors.CYAN,
			"Y":colors.YELLOW,
			"0":colors.DEFAULT,
			"*":"*",	# replace escaped '*' with '*'
		}
	else:
		mapping = {
			"l":LOGGER,
			"m":msg,
			"d":now.strftime("%Y-%m-%d"),
			"t":now.strftime("%H:%M"),
			"*":"*",
		}


	retmsg = ""
	tog = False
	for char in overide:
		if not tog:
			if char == '*':
				tog = True
				continue

		if tog:
			if char in mapping:
				retmsg += mapping[char]
			else:
				if not colorTog:
					retmsg += '*' # replace missing '*'
					retmsg += char

			tog = False
		else:
			retmsg += char


	return retmsg

def out(msg,color="*0"):
	"""
		Synopsis:
			Manual output mode, all of the other functions are essentially
			mutators for this one.
		Notes:
			This is the function you should use to have the most control
			over log formatting, if you just need a simple solution, you
			can simply defer to the basic functions.

			If <WRITE> is True, it will also output to a logfile as well.
		Takes:
			string msg      -	message to output
			string color    -	FORMAT color to use (see FORMSPEC)
		Returns:
			None
	"""
	global OldLogFile
	global LOGFILE
	global logfp
	global isFirstStart
	if isFirstStart: #NOTE: Only way I could think of to do this :/
		OldLogFile = LOGFILE #NOTE: Only to save a HDD read/write
		isFirstStart = False
	if not COLORBLIND:
		print("%s" %genMsg(str(msg),"%s%s*0" %(color,FORMAT)))
	else:
		print("%s" %genMsg(str(msg),"%s" %(FORMAT), True))
	if WRITE:
		if logfp == 0:
			logfp = open(LOGFILE, 'a')
		elif LOGFILE != OldLogFile:
			logfp.close()
			logfp = open(LOGFILE, 'a')
			OldLogFile = LOGFILE
		write(msg, logfp)
		logfp.flush()
	else:
		if logfp != 0:
			try: logfp.close()
			except IOError: logfp = 0

#	colors.norm()
#	colors.reset()

def write(msg,fp):
	"""
		NOTE: DO NOT USE: set <WRITE> and call out() instead.
		Synopsis:
			An alternative to <out()>, writes to an active file pointer
			instead.
		Notes:
			You do not need to remove color flags, as they will be
			automagically culled from FORMAT :D
			Supported flags become the basic data ones:
				*l - logger,
				*m - message,
				*d - date,
				*t - time,
		Takes:
			string msg  - Message to output
			FILE   fp   - file to write to.
		Returns:
			None
	"""
	fp.write("%s\n" %genMsg(msg, FORMAT,True))


def good(msg):
	out(msg,GOOD)

def warn(msg):
	out(msg,WARN)

def error(msg):
	out(msg,ERRO)

def critical(msg):
	out(msg,CRIT)

def notice(msg):
	out(msg,NOTI)

#conveinience functions
log = out
bad = error
