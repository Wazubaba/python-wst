import colors
import sys

def arrow():
	for c in [colors.bblack, colors.green, colors.bgreen, colors.byellow]:
		c()
		sys.stdout.write('>')
	colors.norm()
