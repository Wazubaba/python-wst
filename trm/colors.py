"""
File: colors.py
Author: Wazubaba
Description: Do work Bobby
"""
from sys import stdout
from sys import stderr
from wst.utl.strings import s
import os


""" Set OUT to either OUT or stderr for colored stuff """
OUT = stdout
# Suggestion from tester: provide a flag to bypass this and auto
# assume no PS1
# Report that simply setting DEFAULT to "\x1b[0m" will fix the
# issue. Perhaps just do this immediately? -Suggestion from syn
# try: # NOTE: Need to remove or rework this bit
# 	test = os.environ.get("PS1")
# 	if test != None:
# 		test = test[::-1]
# 		try:
# 			splicePoint = test.find("330\\")
# 			test = test[::-1]
#
# 			test = test[splicePoint*-1:-3]
# 			DEFAULT = "\x1b%s" %test
# 			del splicePoint
# 		except:
# 			DEFAULT = "\x1b[0m"
# 	else:
# 		DEFAULT = "\x1b[0m"
# 	del test
# except:
# 	DEFAULT = "\x1b[0m"
DEFAULT = "\x1b[0m"

del os # Not needed any more

# Static color binding, used for manual insertion
BLACK   = "\x1b[30m"
RED     = "\x1b[31m"
GREEN   = "\x1b[32m"
YELLOW  = "\x1b[33m"
BLUE    = "\x1b[34m"
MAGENTA = "\x1b[35m"
CYAN    = "\x1b[36m"
WHITE   = "\x1b[37m"
BOLD    = ";1m"

def black():
	"""make text black"""
	OUT.write("\x1b[30m")

def red():
	"""make text red"""
	OUT.write("\x1b[31m")
#	 OUT.write("\x1b[31m")

def green():
	"""make text green"""
	OUT.write("\x1b[32m")

def yellow():
	"""make text yellow"""
	OUT.write("\x1b[33m")

def blue():
	"""make text blue"""
	OUT.write("\x1b[34m")

def magenta():
	"""make text magenta"""
	OUT.write("\x1b[35m")

def cyan():
	"""make text cyan"""
	OUT.write("\x1b[36m")

def white():
	"""make text white"""
	OUT.write("\x1b[37m")

def bblack():
	"""make text bold and black"""
	OUT.write("\x1b[30;1m")

def bred():
	"""make text bold and red"""
	OUT.write("\x1b[31;1m")

def bgreen():
	"""make text bold and green"""
	OUT.write("\x1b[32;1m")

def byellow():
	"""make text bold and yellow"""
	OUT.write("\x1b[33;1m")

def bblue():
	"""make text bold and blue"""
	OUT.write("\x1b[34;1m")

def bmagenta():
	"""make text bold and magenta"""
	OUT.write("\x1b[35;1m")

def bcyan():
	"""make text bold and cyan"""
	OUT.write("\x1b[36;1m")

def bwhite():
	"""make text bold and white"""
	OUT.write("\x1b[37;1m")

def norm():
	"""make text normal again"""
	OUT.write(DEFAULT)

def bnorm():
	"""make text normal again but bold this time"""
	OUT.write("\x1b[39;1m")

def reset():
	"""utterly reset all color bits"""
	OUT.write("\x1b[0m")

if __name__ == "__main__":
	byellow()
	print("bold, yellow, and amazing!")
	norm()
	print("and back to normal colors \o/")
