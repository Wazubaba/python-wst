INSTALLATION
============

## L I N U X ##
Installation has been kept as simple as possible. To get
help here, all you have to do is call `make` without any
arguments. If your python libraries are installed to
somewhere other than /usr/lib/python2.7, you can set the
`PREFIX` var to point to it. Ex: `make install
PREFIX=/some/other/python/lib2.7`.

To build the man pages, you need (ronn)[https://github.com/rtomayko/ronn].
Afaik you can install it as long as you have ruby via
`gem install ronn`. If you don't have ruby, you can just
either execute base-install, bypassing the manpages, or
you can install ruby via your package manager/manually.


A new method has been included for those unwilling or unable
to install ruby and or ronn. See prebuilt-manpages for a
bash script to properly install the man pages.

**Warning**: These man pages may be out of date. Will attempt
to keep them as up-to-date as possible though.


As a note, strip is not really needed unless you need to
strip all of the git data and the makefile(for including
this library with your own code for example).

The makefile supports the GNU DESTDIR standard, for ease
of making packages(if you don't know what this is, you
probably do not need to worry about it :) )

## W I N D O W S ##
Not going to lie, I am a linux dev, not a windows one.
That being said, if you want to install this on windows,
you have two options.

1.	You can *try* to use the build.bat script. No guarantees.
At all. Check if pypath is set properly to your python
installation before running.

2. Manually: Continue reading for instructions here...

Prepare for "fun"...

* make a new directory called `wst`

* copy all directories other than expr(unless you *want*
to, recommend against as that stuff is potentially unstable)
into this new wst dir

* copy the __init__.py and (optionally) README.md, contrib.md,
and LICENSE files into the new wst directory

* copy or move that wst directory to wherever python is
installed (I *think* default was C:\Python<versionNumber>\Lib\.)
EX: C:\Python27\Lib\.

* pray

* try this command from a python prompt:
	`from wst.sec import crypt`

If all that worked, then you have successfully installed
the wst library on windows! Now for the last bit of bad news:

**This library is not tested on windows. Use at your own risk
and frustration**

Sorry windows users, but bear in mind, I am all for anyone savvy
enough contributing suggestions(or maybe even an installer? :3)
for fixing things to work on windows.

However, I cannot really test for windows as I'd rather give up
coding altogether than go back to coding on windows...

The alternative is to simply use cygwin to guarantee functionality,
or at least for the install process.

## O S X ##
...I have no clue. Probably similarly to linux.
Anyone that uses this on osx that can tell me the answer will earn
my gratitude and a virtual cookie :P
