This is where I'll list people that have contributed bug fixes, ideas, or even
tests to help expand this library. The order is irrelevant, merely where in
the file I've placed the names :P

* Wazubaba
	Primary Developer and concept creator

* Synergiance
	Bug testing the color module via different terminals
	Potential fix for the module's default escape code behavior
	Typos/formatting issues in README.md

