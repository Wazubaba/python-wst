Wazu STandard library
=====================

## Introduction ##
This is a collection of random modules into a single coherent library.

It is specifically made so I don't need to reinvent the wheel in all
of my projects ;-)

Over time, I shall add to it, and hopefully eventually have something
big here, perhaps it can even challenge the python standard library
for size some day ^^

Please realize this is extremely WIP. I will try not to break backwards
compatibility, but sometimes there is only so much I can do. Documentation
is also a bit sparse at the moment, but I do try to docstring everything
I can as I go. Also please note that this is not developed to replace
any other libraries, nor is it being developed to cater to all developers
ever. It is indeed essentially "Use at your own risk".

If you use this library in something, drop me a line on github, or IRC or
something, and let me know! I would be overjoyed to hear that someone got
use out of it, and may be able to help with any oddness the library could
cause, though I make no guarantees there.


## Features ##
Currently, the following modules all exist:
(Crude ascii graph ahoy!)
Library Name          | Path to library       | Rough completion status

+ Irc Bot Framework   *./net/irc/botcore.py*  [++++++++++] *Depricated*
+ Irc Bot Framework2  *./net/irc/botcore2.py* [++++++++--]
+ Terminal Colors Lib *./trm/colors.py*       [++++++++++]
+ Log Helpers         *./trm/log.py*          [++++++++++] *Depricated*
+ Log Helpers2        *./trm/log2.py*         [++++++++++]
+ Random Ascii Art    *./trm/nifties.py*      [+---------]
+ Async Helper        *./utl/async.py*        [++++------]
+ Shell Helper        *./utl/exec.py*         [++++------]
+ SigInt Helper       *./utl/sigint.py*       [++++++++--]
+ Converter utils     *./utl/converter.py*    [+++-------]
+ File Validation     *./sec/verifyer.py*     [++++++++--]
+ XOR Encrypter       *./sec/crypt.py*        [++++++++--]
+ String Utils        *./utl/string.py*       [++++++----]
+ Config Util         *./fio/cfg.py*          [+++++-----]
+ ArgParse Util       *./trm/argparse.py*     [++++++++--]
+ Simple Tokenizer    *./utl/tokenizer.py*    [+++++++---]
+ Simply html helpers *./utl/htmlconsts.py*   [++++++----]

I won't deny that there may be a few I did not list because I forgot I
have them...


## Utilities ##
There are currently two utilities available.

First, there is a projectInfo program, which will take files as arguments and
output linecount and total number of files processed.

Second, there is a helper program called statistics, which sort of acts as an
interface to projectInfo, and walks the current working directory for files
which then get fed as arguments to projectInfo.

See tools/projectInfo --help for more information on how to use it :)

#### Important ####
Please note, for the statistics utility to work, you *must* have the
projectInfo utility in your path, or else it *will* fail.


## Documentation ##
See the in-line doc strings for each module, as well as any entries it
may have in the *./doc/* directory tree. Each doc will be a markdown
file, so it should be child's play to convert to an html form.

See [MarkDown Documentation](http://daringfireball.net/projects/markdown/basics)
for more info.

In the future, I will probably include a simple way to convert the markdown
to html via a makefile rule.


## Installation ##
Okay, now that you either skipped all the above(Which means it isn't my fault if
everything explodes into a glorious fireball =^^=), or actually read it and
understand exactly what you are getting yourself into, head on over to the
INSTALL.md file for instructions.
