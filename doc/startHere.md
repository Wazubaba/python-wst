   Wazu Core
====================
##standard library##

This is a collection of random modules into a single coherant library.

It is specifically made so I don't need to reinvent the wheel in all
of my projects ;-)

Over time, I shall add to it, and hopefully eventually have something
big here, perhaps it can even challenge the python standard library
for size some day ^^

##documentation##
See the in-line doc strings for each module. Other than that, there
are general notes for each module, with examples and a basic run-down
of usage. I am hoping that I made the modules well enough that you can
learn the usage+api from the docstrings, but of course I can't always
be sure!

Every file in here is written in *markdown* syntax. This means it is:
*	Simple to read (some editors may even have wonderful syntax
	highlighting for it \o/

*	Can easilly be converted to html, for a website-like experience.

See [MarkDown Documentation](http://daringfireball.net/projects/markdown/basics)
for more info.

In the future, I will probably include a simple way to convert the markdown
to html via a makefile rule, but for now you are on your own for it :P

