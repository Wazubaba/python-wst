wst-log2(3WST) -- Simple logging library for Python
===============================================

## SYNOPSIS
`# See the section on` [mark up formatting][FORMAT LANGUAGE SPECIFICATION] `for more details on` <formatmarkup>  
from wst.trm import log2

`# Global Variables`

`LOGGER     = str` <name>  
`FORMAT     = str` <formatmarkup>  
`STDOUT     = bool` <toggle>  
`LOGFILE    = str` <filename>  
`LOGFORMAT  = str` <formatmarkup>

`# Functions`

`fmtParse(str` <msg>`, str` <format="">`, bool` <enableColors=STDOUT>`)`
`out(str` <msg>`, str` <override=FORMAT>`, str` <logfileOverride=LOGFORMAT>`, bool` <enableColors=STDOUT>`)`

## DESCRIPTION
The global variables control how the logger operates.
`out()` will only write to stdout if STDOUT is True for example.
If `LOGFILE` is specified, a logfile with that name will be created.
`out()` will write to that logfile, opening it for appending each time,
culling the color mapping so as not to clutter up up the logfiles.

## FORMAT LANGUAGE SPECIFICATION
The following chars when placed after an asterisk(\*) will
be replaced with the matching data:

-	l	-	logger
-	m	-	message
-	d	-	date
-	t	-	time

Basic colors are also supported:

-	G	-	Green
-	R	-	Red
-	B	-	Blue
-	X	-	Black
-	M	-	Magenta
-	W	-	White
-	C	-	Cyan
-	Y	-	Yellow
-	0	-	Reset colors to terminal default

## SEE ALSO
**python(1), wst(3)**

## AUTHOR
Wazubaba (noEmailYet)
