wst(3WST) -- WST module Manual Front-End
========================================

## SYNOPSIS
`import wst`

## DESCRIPTION
WST is a large library of different modules for python, which serve extremely
varied purposes.

## SEE ALSO
**python(1), wst-log(3), wst-config(3)**

## AUTHOR
Wazubaba (noEmailYet)
