wst-log(3WST) -- Simple logging library for Python
===============================================

## SYNOPSIS
`# See the section on` [mark up formatting][FORMAT LANGUAGE SPECIFICATION] `for more details on` <formatmarkup>  
from wst.trm import log

`# Global Variables`

`LOGGER     = str` <name>  
`FORMAT     = str` <formatmarkup>  
`LOGFILE    = str` <filename>  
`COLORBLIND = bool` <toggle>  
`WRITE      = bool` <toggle>  
`GOOD       = str` <formatmarkup>  
`WARN       = str` <formatmarkup>  
`ERRO       = str` <formatmarkup>  
`CRIT       = str` <formatmarkup>  
`NOTI       = str` <formatmarkup>  

`# Functions`

`genMsg(str` <msg>`, str` <override="">`, bool` <disableColors=False>`)`  
`out(str` <msg>`, str` <color="\*0">`)`  
`write(str` <msg>`, FILE` <file>`)`  
`good(str` <msg>`)`  
`warn(str` <msg>`)`  
`error(str` <msg>`)`  
`critical(str` <msg>`)`  
`notice(str` <msg>`)`

`# Macro Functions`

`log = out()`  
`bad = error()`  

## DESCRIPTION
These functions will write the string <msg> to stdout, and optionally a file if
`WRITE` is set to `True`, save for write, which will instead write them to
FILE <file> instead.

## FORMAT LANGUAGE SPECIFICATION
The following chars when placed after an asterisk(\*) will
be replaced with the matching data:

-	l	-	logger
-	m	-	message
-	d	-	date
-	t	-	time

As an example, the preset format is `[\*l:\*m]`.

Basic colors are also supported:

-	G	-	Green
-	R	-	Red
-	B	-	Blue
-	X	-	Black
-	M	-	Magenta
-	W	-	White
-	C	-	Cyan
-	Y	-	Yellow
-	0	-	Reset colors to terminal default

**Please note:**  
Multiple colors in a log message is very fun and
great, but the vast majority of this module
automatically assigns colors, so if you intend to do
anything really nifty, you should probably stick to
manually calling `out(`<msg>`)` so as to not have odd
effects :D

## SEE ALSO
**python(1), wst(3)**

## AUTHOR
Wazubaba (noEmailYet)
