wst-config(3WST) -- Simple library for config parsing
====================================================

## SYNOPSIS
`# Classes`

`class Cfg`  
`Cfg.open(str` <filename>`, bool` <warn=True>`, bool` <strict=False>`)`  
`str Cfg.get(str` <key>`)`  

## DESCRIPTION
After instantiating the class `Cfg`, you need only call <instance>`.open(`<path>`)`
and start querying it for keys from the cfg file with <instance>`.get(`<key>`)`.

## NOTE
`Cfg.get(`<key>`)` will return a string value. Bear that in mind and ensure you
alter the type to whatever you need.

## SEE ALSO
**python(1), wst(3), wst-config(5)**

## AUTHOR
Wazubaba (noEmailYet)
