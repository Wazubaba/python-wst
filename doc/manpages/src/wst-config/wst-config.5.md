wst-config(3WST) -- Simple library for config parsing
====================================================

## SYNTAX
`# comment`  
<key> `=` <value>  
<key> `=` <value>  

<key> `=` <value>  
`.v.etc.v.`

## DESCRIPTION
This is the format that wst-config uses as a configuration file.

## SEE ALSO
**python(1), wst(3), wst-config(3)**

## AUTHOR
Wazubaba (noEmailYet)
