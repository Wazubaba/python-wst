wst.net.irc - botcore
=====================
This is a simplified inheritable class that is hopefully
a turn-key solution to writing IRC bots. You pretty much
only need to supply the command processing :D

##Basic Use##
You would start out by creating a class, which inherits
the core, and call the botcore's init function, handing
it the pointer to your class(i.e. self), and a dict with
the following keys:

*	"name"	-	botname
*	"serv"	-	server to connect to
*	"chan"	-	channel to join upon connecting
*	"port"	-	port to use during connection

As an example: `botcore.ircbot.__init__(self,info)`

After that, you can simply add a function to your class
that takes no arguments(except for self, of course :P),
and process your commands via testing the following data:

*	self.msg	-	the message parsed from chat
*	self.usr	-	the user who issued the message
*	self.chn	-	the channel of the user
*	self.hst	-	the hostname of the user

The name of this function should be `handlers`.
See *./doc/examples/ircbot.py* for a fully functional
irc bot using this system :D
