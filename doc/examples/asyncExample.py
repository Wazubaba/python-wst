from wst.utl.async import asyncore
from sys import stdin
from wst.trm import log

class test(asyncore):
	def process(self,interface):
		if interface == stdin:
			cmd = interface.readline().strip("\r\n")
			if cmd == 'q':
				self.running = False
				log.log("Shutting down reactor...")
			else:
				log.good(cmd)


log.LOGGER = "asyncExample"	

monitors = {
	"readers":[stdin],
	"writers":[],
	"excepts":[],
}

log.notice("Activating reactor, give the command 'q' to shut down.")
fish = test(monitors)
fish.start()



