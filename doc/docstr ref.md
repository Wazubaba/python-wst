Docstring Formatting Reference
==============================

I have come up with a general format for docstrings
to sort of __standardize__ them into one cohesive
format that should properly explain everything fully
in a simple way.

##Layout##
The docstrings are organized into sections similar to
linux man pages. In fact, I have thoughts to generate
man pages from these docstrings :P

The sections are as follows, in roughly this order:
1	Synopsis	-	Short description of the function
2	Notes		-	(OPTIONAL)Any explanation of complicated things go here
3	Takes		-	Arguments for the function, with an asterisk next to a required argument
4	Returns		-	What the function returns

##Formatting##
*	square brackets		-	class functions(methods)
*	angled brackets		-	arguments to a function
*	aterisks			-	emphasis, important info


