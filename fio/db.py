import zlib
import struct

# NOTE: As far as I recall, this is broken and I somewhat forgot it
# existed, will try to get working eventually as soon as I need it
# :P

class db(object):
	def __init__(self):
		self.data = {}
		pass

# NOTE:
# better idea for the format:
#
# what if we stored multiple dicts worth of crap inside a list?
#
# layout:
# int num of dicts
# string name of dict 1 (also serves as the marker for beginning of read of data)
# int number of keys
# int len key 0's list of data
# key 0 data
# int len key 1's list of data
# key 1 data
# int len key 2's list of data
# key 2 data
# ...etc...
# string name of dict 2
# ---continues till last entry---
#
# NOTE: Perform a test to verify if this wouldn't be better compressed
# (or more legible even) if stored as raw-text and then compressed via
# zlib... wait forgot, newlines and any other formatting of text would
# fuck it up... forget it :P
#
# NOTE: Think of this as a stored key->val array. Like a hash map from
# good ol' construct :P
#
# NOTE: Also, this would basically be a list of 2 val tuples, with first
# being a string containing the name of the tree, and the second being a
# dict containing the data.

	def compile(self, key, data):
		s = struct.Struct(key)
		retval = s.pack(data)
		return retval

	def decompile(self, key, data):
		s = struct.Struct(key)
		retval = s.unpack(data)
		return retval

	def test(self):
		work = []
		numTrees = len(self.data)-1 # -1 because we start from index 0 on iteration
		work.append(self.compile("i", numTrees))# int num of dicts
		for tree in self.data:
			work.append(self.compile("s", tree[0])) # string name of dict
			work.append(self.compile("i", len(tree[1])-1)) # int number of keys
			for key in tree[1]: # Iterate through the data
				work.append(self.compile("s", key)) # string key name
				work.append(self.compile("i", len(tree[1][key])-1)) # int len data
				work.append(self.compile("%ss" %(len(tree[1][key])-1), tree[1][key])) # Append the data for the data
		# And that should theoretically do it...
		# return the value
		self.data = work

	def testDecode(self):
		# First, we need to reconstruct our container list...
		work = []
		point = self.data[0]
		treeOffset = 1 # for figuring out where to read stuff from...
		for treeIndex in range(self.decompile("i", point)[0]): # Iterate through the trees
			tree = ["",{}]
			tree[0] = self.decompile("s", self.data[treeOffset])[0]
			treeOffset += 1
			for branch in range(self.decompile("i", self.data[treeOffset])[0]):
				treeKey = self.decompile("s", self.data[treeOffset+1])[0] # get the key 
				treeDatLen = self.decompile("i", self.data[treeOffset+2])[0] # get the len of the data
				tree[1][treeKey] = self.decompile("%ss" %(treeDatLen), self.data[treeOffset+3]) # finally reconstruct the dict
				treeOffset += 4 # Finally increment our offset to the next key

			work.append(tree)
			treeOffset += 1 # Increment to the next tree


	def printDatabase(self):
		for tree in self.data:
			print(tree[0])

	def encode(self):
		compileKey = ""
		retval = []
		for key in self.data:
			# Going to comment this as it is weird looking imo :/
			size = len(self.data[key]) # Get the size of our dict's current key's list of data

			s = struct.Struct("I") # Compile our first key, which will always be an int specifying len of the data
			retval.append(s.pack(size)) # Wire in the count

			s = struct.Struct("%ss" %size) # Compile the struct for the data
			retval.append(s.pack(*self.data[key])) # Finally append the encoded data into our temp array

		s = struct.Struct("i")
		retval.append(s.pack(0)) # Append this so we know we are at EOF

		# Cleanup that may or may not be needed but why the fuck not?
		try:
			del s
			del compileKey
			del size
		except: pass

		self.data = retval
		del retval

	def decode(self):
		pass



	def write(self, fname = ""):
		if fname != "":
			self.fname = fname
		self.fh = open(self.fname, "wb")
		self.fh.write(zlib.compress(self.data))
		self.fh.close()
		if LOGGING:
			log.out("Wrote to %s successfully!" %self.fname)

	def open(self, fname = ""):
		self.fname = fname
		
		try:
			self.data = zlib.decompress(open(self.fname, "rb").read())
		except IOError:
			if LOGGING:
				log.out("Error opening file '%s'" %self.fname)

# File Example:
# size of list as int
# list of strings according to size
# size of next list, if 0 then EOF
# next list unless EOF
# ^loops untill EOF^



		
LOGGING = True
if LOGGING:
	from wst.trm import log
	log.LOGGER = "wst.fio.db"
	log.FORMAT = "*W[ *R*l*W ] *m*0"


#XXX: Debug Code
if __name__ == "__main__":

	print("Testing database system...")
	print(".:Initial Test, Generating Test File:.")
#	testdat = "Dogwater catching\nfish\n\n\tTWICE AUGH"
	testdat = [
		("db01",{
			"key": "value",
			"water": "melon",
		}),
		("dogs database",{
			"fish": "water",
			"cat" : "nya",
			"dog" : "bark",
		}),
	]
#	print("\tData sample:\n##########START##########\n%s\n##########E#O#F##########" %testdat)

	testdb = db()
	print("\n.:Injecting Data Into Database:.")
	testdb.data = testdat
	testdb.test()
	print(".:Encoding complete:.")
	print(".:Now attempting decode:.\n")
	testdb.testDecode()
	testdb.printDatabase()
#	print("\n.:Attempting Write To File:.")
#	testdb.write("testFile.db")
#	print(".:Write Complete:.")
#	print("\n.:Instantiating New Clone And Attempting Data Load:.")

#	del testdb
#	testdb = db()
#	testdb.open("testFile.db")
#	print(".:Read Complete:.")
#	try:
#		print("[DATA]:%s" %(testdb.data))
#		print("\n\n.:Test Complete:.")
#	except:
#		print(".:Unable To Load Data:.")


