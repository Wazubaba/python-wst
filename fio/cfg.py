from wst.utl.strings import s

class Cfg():
	"""
		Synopsis:
			Class for storing and handling the config file format.
		Notes:
			After creating an instance of a config, simply call
			[open] (see docstring below) to load and parse a cfg
			file (if possible).
	"""

	def __init__(self):
		self.data = {}

	def open(self, fn="", warn=True, strict=False):
		"""
			Synopsis:
				Opens and attempts to read a config file, stores
				results in instance's [data] dict.
			Takes:
				string fn	-	full path and filename (ex: /etc/myAmazingThingy.cfg)
				bool warn	-	Display wst.trm.log warnings
				bool strict	-	If an error in the config file is found, raise an error.
			Returns:
				None
		"""
		if fn == "": raise IOError

		lineNo = 0

		try:fp = open(fn,'r')
		except IOError:
			if warn: print("wst-config: Unable to load %s!" %fn)
			if strict: raise IOError
			else: return

		for line in fp.readlines():
			lineNo += 1
			if line[0] == '/':
				if line[1] == '/':
					continue # this would be a comment
			if line[0] in ['\r','\n']: continue # this ought to be a blank line


			try:
				temp = s(line).split('=')
				if len(temp) < 1: continue

				# purge whitespace off sides and new lines(even window's \r :P)
				temp[0] = s(temp[0]).trimSpaces()
#				 temp[0] = s(temp[0]).purge(' ',1).rpurge(' ',1) # if there is a newline here its your fault :P
#				 temp[1] = s(temp[1]).purge(' ',1).rpurge(' ',1).purge('\n')
				temp[1] = s(temp[1]).trimSpaces().purge('\r').purge('\n')

				if temp[0] in self.data:
					if warn:
						self.err("Duplicate key \"%s\" @ line %d!" %(temp[0],lineNo))
					if strict: raise KeyError
				self.data[temp[0].data] = temp[1].data
			except:
				if warn:
					self.err("Possibly malformed config \"%s\" @ line %d!" %(fn,lineNo))
				if not strict: continue # try to keep moving through the data unless using strict.

	def get(self,key):
		"""
			Synopsis:
				Simple helper for getting a key if it exists.
			Takes:
				string key	-	key to test for
			Returns:
				If there is a matching key in the self.data[dict], return value.
				Else returns None
		"""
		if key in self.data:
			return self.data[key]
		else:
			return None

if __name__ == "__main__":
	meow = Cfg()

	meow.open("sample.cfg")
	for k in meow.data:
		print("%s = %s" %(k,meow.data[k]))
