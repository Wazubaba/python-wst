#   Name        Mod System Library
#
#   Author      Wazubaba
#
#   Purpose     Allow modules to be loaded and
#               unloaded dynamically
#
########################################################

def doWhatISayYouFuck(name):
    """
        Antiquated and Obsolete Thesis for the class.

    """
    mod = __import__("mod%s" %name)
    return mod

import sys

class system:
    """
        The main system class, which maintains the mod
        structure.

    """
    def __init__(self,safe=True):
        """
            main init function for the class.
            the safe toggle controls whether
            a purge and an init function must
            be found for the module to be
            loaded.

        """
        self.mods = {}
        self.safe = safe

#    def add(self,name,nick=""):
    def add(self,name): #nick naming is unstable atm
        """
            loads a new module, performing tests as
            necessary to determine whether a module
            is safe or not. As a warning, currently
            you MUST have any mods in the same
            directory as the main script!
        """
#            nick is what the module will be referenced
#            as in the main module dictionary.
#
#        """
#        if nick != "": modname = nick
#        else: modname = name
        modname = name

        try:
            self.mods[modname] = __import__("%s" %name)

        # verify that there is a delete hook
            if self.safe:
                if not callable(getattr(self.mods[modname],"purge")):
                    print("[system:missing exitpoint, unloading %s to protect against memleaks!]" %name)
                    self.rem(modname)
                    return

                self.mods[modname].init()
        except ImportError,msg:
            print("[system:unable to load %s]" %modname)
        except AttributeError,msg:
            print("[system:Warning: failed to load %s]" %modname) # change this block if you actually DO want to load it
            print("Debug:%s" %msg)
            self.rem(modname)

    def reload(self,name):
        """
            convieniance function, reloads a module
            so that any changes are active.

        """
        if name in self.mods:
            reload(self.mods[name])

    def rem(self,name):
        """
            unload a module

        """
        if name in self.mods:
            try:
                self.mods[name].purge()
            except AttributeError,msg:
                print("[system:Warning: %s is missing exitpoint! god save you if there are any imports for it o_o]" %name)
                print("Debug:%s" %msg)
            try:            
                if self.mods[name].imports:
                    for lib in self.mods[name].imports:
                        del sys.modules[lib]
            except AttributeError,msg:
                print("[system:CRITICAL: %s doesn't have an imports table, this means that it may have not completely unloaded any modules it imported, which means they will not be cleaned up. This is a memory leak untill you either purge them yourself or restart the program.]" %name)
                print("Debug:%s" %msg)

            del self.mods[name]
            del sys.modules["%s" %(name)]

    def probe(self):
        """
            A debug function, prints a list
            of currently loaded modules

        """
        for mod in sys.modules:
            print(mod)

###################
#test harness code#
###################

#doWhatISayYouFuck("test").test("fish")
#
#print("-=make edit now=-")
#raw_input()
#doWhatISayYouFuck("test").test("fish")


#probe = False # debug toggle
#
#test = modtest
#test = "samplemods.modtest" #not working till\
#we can take care of the issue of the . expansion...
#
#this = system()
#this.add(test,)
#
#
#this.mods["samplemods.modtest"].test("fish")
#if probe:this.probe()
#
#this.rem(test)
#if probe:
#    print("\n\n")
#    this.probe()


#this.mods["test"].test("blah")
#
#print("-=make edit now=-")
#raw_input()
#this.reload("test")
#this.mods["test"].test("blah")
#this.rem("test")
#
#try:
#    this.mods["test"].test("blah")
#except KeyError,msg:
#    print(str(msg))
#
#print("-=Make an edit now=-")
#raw_input()
#this.add("test")
#this.mods["test"].test("blah")

