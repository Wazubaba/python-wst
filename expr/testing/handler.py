
def hi(arg):
    return "this:%s" %(arg)

def showhelp(arg):
    for key in mapping:
        print("%s" %key)

def e(arg):
    exit()

def add(arg):
    """
        with this, we could offload the mapping dict to a seperate file. or better,
        read a file specified by arg, which contains a function and an "access list",
        or a dictionary called _mapping that contains text pointing to the functions.

        idea:
            "key":(["arg types"],function pointer)
    """
    
mapping = {
        "hi":hi,
        "help":showhelp,
        "e":e
        }


while 1:
    cmd = raw_input("cmd:")
    arg = "fish"
    if cmd in mapping:
        mapping[cmd](arg)
    else:
        print("cmd does not exist")



