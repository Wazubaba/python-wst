from wst.trm import colors
from wst.utl import strings
import datetime

"""
	FORMAT Specfication
	
	The following chars when placed after an aterisk(*) will
	be replaced with the matching data:

		l	-	logger
		m	-	message
		d	-	date
		t	-	time
	
	As an example, the preset format is `[*l:*m]`.
"""

FORMAT = "[*l:*m]"
LOGGER = "CHANGEME"

def genMsg(msg):
	now = datetime.datetime.now()
	mapping = {
		"l":LOGGER,
		"m":msg,
		"d":now.strftime("%Y-%m-%d"),
		"t":now.strftime("%H:%M"),
	}

	retmsg = ""
	tog = False
	for char in FORMAT:
		if char == '*':
			tog = True
			continue
		
		if tog:
			if char in mapping:
				retmsg += mapping[char]
			else:
				retmsg += '*' # replace missing '*'
				retmsg += char
			tog = False
		else:
			retmsg += char
	return retmsg


print(genMsg("...yelhai..."))


