import socket
from time import sleep

class BotCore(object):
	"""
		Main IRC bot class, to use this you should
		inherit it into a base class in your code.

		It is hopefully pretty friendly to work
		with, in that all you really need to do is
		provide a `handlers` function, which will
		test the values stored in [msg], [usr],
		[chn], and [hst] and do different things
		based upon the input.

		Initial configuration is done via a dict
		that should have the following keys:

		    name  -  name of the bot
		    serv  -  name of the server
		    chan  -  name of the channel
		    port  -  port to connect on

		Please note, all but the port key should be
		strings, with port being an int.

	"""
	def __init__(self, info={}):
		# Wire-in bot IRC info
		self.name = info["name"]
		self.serv = info["serv"]
		self.chan = info["chan"]
		self.port = info["port"]

		# attempt to translate port into a number in case it comes in as a string
		if type(self.port) != "int": self.port = int(self.port)

		# [active] determines if [main] gets run or not
		self.active = True

		# FIXME: Try to do away with needing to preset the data var
#		self.data = ""

		# IRC socket
		self.sock = socket.socket()

		# Vars to track reconnect attempts
		self.retries = 0
		self.suspend = 0

		# Fire up the main loop
		self.main()

	def main(self):
		#Main loop
		while self.active:
			try:
				if self.state > 0:
					try:
						if self.data.find("PING") != -1:
							self.sock.send("PONG %s\r\n" %(self.data.split()[1]))
					except:
						pass
			except:
				self.state = 0  #XXX: Attempt to fix, theoretically this can only
								#fail if there is no connection, as otherwise [data]
								#would already exist :P

			try:
				if self.data == "": self.state = 0
			except: pass

			# Handle incrementing our connection counters
			if self.state == 0:
				self.retries += 1
				if self.retries == 5:
					time.sleep(60)
					self.suspend += 1
					self.retries = 0
				if self.suspend == 5:
					time.sleep(60*30) # Half an hour
					self.suspend = 0

				self.sock.connect((self.serv, self.port))
				self.state = 1
				#FIXME: Try to get rid of the need to predefine [data]
#				self.data = "TEMP"

			elif self.state == 1:
				# Try to ident to the server
				self.data = self.sock.recv(256)
				self.sock.send("NICK %s\r\nUSER %s %s %s :%s\r\n" %(self.name, self.name, self.name, self.name, self.name))
				self.state = 2

			elif self.state == 2:
				self.data = self.sock.recv(1024)
				if not self.data:
					self.state = 1
					continue

				print(self.data)

				if self.data.find("252") != -1:
					pass #TODO: Make a nice read-out for the MOTD

				if self.data.find("376") != -1:
					print("%s-> I got connected to %s!" %(self.name, self.serv))

					# Reset attempts
					self.retries = 0
					self.suspend = 0
					
					self.sock.send("JOIN %s\r\n" %self.chan)
					self.state = 3

			elif self.state == 3:
				self.data = self.sock.recv(512)

#				print(self.data)
				self.parse()
				if self.data.find("PRIVMSG") != -1:
					self.handlers()

	def parse(self):
		#NOTE: This is not a block for the faint of heart...
		if self.data.find("PRIVMSG") != -1:
			try: self.msg = self.data.split("PRIVMSG",1)[1].split(':',1)[1].strip("\r\n")
			except: self.msg = "nullmsg"

			try: self.usr = self.data.split('!')[0][1:]
			except: self.usr = "nullusr"

			try:
				if self.data.split()[2].find('#') != -1: self.chn = self.data.split()[2]
				if self.chn[0] == ':': self.chn = self.chn[1:]
			except: self.chn = "nullchn"

			try: self.hst = self.data.split()[0].split('!')[1].split('@')[1]
			except: self.hst = "nullhst"

			try:
				if self.data.split()[2].find("#") != -1: self.chn = self.data.split()[2]
				if self.chn[0] == ':': self.chn = self.chn[1:]
			except: self.chn = "nullchn"

#######################################################
# Everything below this point is a `helper` function  #
# They are included for ease of use and compatibility #

	def send(self, msg, method, target=""):
		if method == 0:
			self.sock.send("NOTICE %s :%s\r\n" %(target,msg))
		elif method == 1:
			self.sock.send("PRIVMSG %s :%s\r\n" %(target, msg))


	def say(self, msg, target=""):
		if target == "": target = self.chn
#		print("%s->Saying %s to %s~" %(self.name, msg, target))
		self.send(msg, 1, target)

	def tell(self, msg, target=""):
		if target == "": target = self.chn
#		print("%s->Noticing %s to %s~" %(self.name, msg, target))
		self.send(msg, 0, target)

