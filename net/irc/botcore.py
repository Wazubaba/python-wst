import socket
import time


class ircbot(object):
	"""
		The main botcore class. You should inherit this
		into your own bot class, then call the init
		function of this class, as well as specify a
		function called "handlers".
		
		See documentation for a better description, and
		for an example!
	"""
	def __init__(self,info={}):
		print("CHEESE")
		self.name = info["name"]
		self.serv = info["serv"]
		self.chan = info["chan"]
		self.port = info["port"]
#	self.name = "none"
#		 self.serv = self.name
#		 self.chan = self.name
#		 self.port = 0
		self.active = True
		self.data = ""

		self.sock = socket.socket()
		self.retries = 0
		self.giveupandwaitcounter = 0

		self.main()

	#HELPERS#
	#########
	def say(self,msg,target=""):
		"""
			Helper function, broadcasts <msg> to the
			current channel, or to <target> if specified.

			Takes:
				self
				string msg
				string target
			Returns:
				nothing
		"""
		if target == "":target=self.chn
		if target == "nullchn":
			print "%s->Wazzzzyy! You tell me how to parse the channel right! >_<"
			return
		print "%s->Saying %s to %s~" %(self.name,msg,target)
		print "PRIVMSG %s :%s\r\n" %(target,msg)
		self.sock.send("PRIVMSG %s :%s\r\n" %(target,msg))

###############################################################################
	def main(self):
		"""
			Main control function
		"""
		while self.active:
			if self.data.find("PING") != -1:
				self.sock.send("PONG %s\r\n" %(self.data.split()[1]))

			if self.data== "": self.state = 0

			if self.state == 0:
				self.retries += 1
				if self.retries > 5:
					time.sleep(60)
					self.giveupandwaitcounter += 1
					self.retries = 0
				if self.giveupandwaitcounter > 5:
					time.sleep(60*30) #sleep for a half hour
					self.giveupandwaitcounter = 0
				self.sock.connect((self.serv,self.port))
				self.state = 1
				self.data = "TEMP"

			elif self.state == 1:
				self.data=self.sock.recv(256)
				self.sock.send("NICK %s\r\nUSER %s %s %s :%s\r\n" %(self.name,self.name,self.name,self.name,self.name))
				self.state = 2

			elif self.state == 2:
				self.data=self.sock.recv(1024)
				print self.data
				if self.data.find("252") != -1:
					pass #todo, make a nice display for motd

				if self.data.find("376") != -1:
					print "%s-> I got connected to %s!" %(self.name,self.serv)
					self.retries = 0
					self.giveupandwaitcounter = 0
					self.sock.send("JOIN %s\r\n" %self.chan)
					self.state = 3

			elif self.state == 3:
				self.data = self.sock.recv(512)
				print self.data
				self.parse()
				self.handlers()

	def parse(self):
		"""
			Raw data parsing function. This splits the raw
			stream into the 4 message buffer portions:
				msg
				usr
				chn
				hst
		"""
	# this ugly bit parses the data for the handler function
		if self.data.find("PRIVMSG"):
			try: self.msg = self.data.rsplit(":",1)[1].strip("\r\n")
			except: self.msg = 'nullmsg'
			try: self.usr = self.data.split('!')[0][1:]
			except: self.usr = 'nullusr'
			try:
				if self.data.split()[2].find("#") != -1:self.chn = self.data.split()[2]
				if self.chn[0]==":": self.chn=self.chn[1:]
			except: self.chn = 'nullchn'
			try: self.hst = self.data.split()[0].split('!')[1].split('@')[1]
			except: self.hst = 'nullhst'

			#self.data = ""
			if self.chn != 'nullchn' and self.msg != 'nullmsg': print "%s|<%s>%s" %(self.chn,self.usr,self.msg)
			if self.msg == "": self.msg = "nullmsg"
