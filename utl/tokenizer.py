__doc__ = """
	Tokenizer Framework v1
	This hopefully will be able to work as a modular text tokenizer,
	capable of taking a dictionary containing char's mapped to functions
	and executing functions given a copy of the data and the index of the
	hit.

	As a heads up, this is not really that advanced. You are probably
	better off implementing a custom one of your own if you need anying
	really advanced :/
"""

class Tokenizer(object):
	"""
		The main Tokenizer class
	"""
	def __init__(self, funcmap = {}, marker="*"):
		"""
			Synopsis:
				Init Sector of Tokenizer class.
			Takes:
				dict funcmap	-	chars mapped to functions
				marker (opt)	-	marker to use to denote token start
			Returns:
				None
		"""

		self.map = funcmap
		self.mark = marker

	def parse(self, data):
		"""
			Synopsis:
				This is the main function for the Tokenizer class, it will
				parse a given string data, and execute the associated
				functions given as a funcmap.
			Takes:
				string data		-	string to parse
			Returns:
				None
		"""
		state = 0
		for i in range(len(data)-1):
			if state == 0:
				if data[i] == self.mark:
					state = 1
			elif state == 1:
				if data[i] == self.mark:
					state = 0
				else:
					try:
						self.map[data[i]](data, i)
					except:
						pass
					finally:
						state = 0


if __name__ == "__main__":
	def rawr(_,__):
		print("hi")
	test = {
		"w" : rawr
	}
	tokenizer = Tokenizer(test)
	tokenizer.parse("rawr*wfish")
