import os

class cmder(object):
	"""
		Simple helper for executing commands, basically
		to reduce code quantity
	"""
	def __init__(self,cmd="",data=[]):
		self.cmd = ""
		self.out = []
		self.err = []
		if cmd != "":
			self.run(cmd,data)

	def run(self,cmd,data=[]):
		""" Execute <cmd> with <data> """
		stdin, stdout, stderr = os.popen3(cmd)
		self.out = stdout.readlines()
		self.err = stderr.readlines()

		if data != []:
			for line in data:
				stdin.write(line)

if __name__ == "__main__":
	""" Sample """
	test = cmder("ls -l")
	for i in test.out:
		print(i)
