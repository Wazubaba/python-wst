__doc__ = """
	Converter module
	A collection of convienance functions for quick variable conversion.
"""
	
def list2str(data, padding = ''):
	"""
		Synopsis:
			Converts a list into a string with optional <padding>.
		Takes:
			list data		-	data to convert
			string padding	-	optional: char (or string) to pad the strings with
		Returns:
			string retval	-	Stringified contents of the list
	"""
	if type(data) != "list": raise TypeError
	retval = ""
	for key in data:
		retval += str(key)+padding
	return retval

