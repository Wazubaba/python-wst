
__doc__ = """
	Mod Module.
	This is a system to allow for dynamic "hot-swapping" of python modules. It
	is hopefully simple to work with, and generally safe so long as all
	guidelines are followed.
"""
