__doc__ = """
This is a simple helper containing some useful functions and a mapping
of valid html symbols. It is primarilly intended for use in programs
that need to generate valid html.
"""

def endify(symbol):
	"""
		synopsis:
			return a string with a properly closed html symbol
		takes:
			string symbol    -    a string containing the html symbol
		returns:
			string result    -    a closed html symbol
	"""
	return "</" + symbol[1:]

def oneLineify(symbol):
	"""
		synopsis:
			return a string with a properly closed one-liner html symbol (ex = <hr />)
		takes:
			string symbol    -    a string containing the html symbol
		returns:
			string result    -    a one-liner html symbol
	"""
	return symbol[:-1] + " />"

class htmlSymbols:
	"""
		Using a class to store these because dot notation is less typing than dict indexing
	"""
	def __init__(self):
		"""
			Synopsis:
				Stores the proper symbols as variables.
			Notes:
				Valid symbols:
				 html
				 p
				 h1 - h6
				 b
				 u
				 hr
		"""
			 
		self.html = "<html>"
		self.p = "<p>"
		self.h1 = "<h1>"
		self.h2 = "<h2>"
		self.h3 = "<h3>"
		self.h4 = "<h4>"
		self.h5 = "<h5>"
		self.h6 = "<h6>"
		self.b =  "<b>"
		self.u =  "<u>"
		self.hr = "<hr>"

