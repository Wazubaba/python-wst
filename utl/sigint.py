import signal
from sys import exit

def sigInt(signal, frame):
	print("SIGINT recieved, shutting down...")
	exit(0)

signal.signal(signal.SIGINT, sigInt)

