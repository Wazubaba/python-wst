import select

class asyncore(object):
	"""
		Synopsis:
			An asynchronous processing loop.
		Takes:
			dict monitors:
				list readers	-	input sources to watch
				list writers	-	things to write to
				list excepts	-	... dunno yet
		Returns:
			None
	"""
	def __init__(self, monitors = {}):
		if monitors == {}:
			self.monitors = {
				"readers":[],
				"writers":[],
				"excepts":[],
			}
		else:
			self.monitors = monitors

	def start(self):
		"""
			Synopsis:
				Start the asynchronous reactor.
			Notes:
				Whenever something pointed to in [monitors] is abled to
				have IO performed on it, the reactor will call [process]
				with an argument of the file pointer. From there you can
				simply perform normal file functions.
			Takes:
				None
			Returns:
				None
		"""
		self.running = True
		while self.running:
			try:
				iready, oready, eready = select.select(self.monitors["readers"], self.monitors["writers"], self.monitors["excepts"])
#			iready, oready, eready = select.select(self.monitors["readers"], self.monitors["writers"], self.monitors["excepts"])

				for interface in iready:
					self.process(interface)
					
			except select.error, e:
				self.running = False

