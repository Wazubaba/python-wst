"""
	String Module
	Due to the nature of python, I cannot simply extend the
	core string class. I find this rediculous. Therefore, I am
	doing up my own bloody class for it. It will have all of the
	same features as standard strings, and be fully reverse-
	compatible. Each function will both alter the original String
	(semi-mutable string. Yes), and return a copy as the core string
	object does. !!!VERY WIP!!!
"""

class s(object):
	def __init__(self,baseVal = ""):
		self.data = baseVal

	def delch(self,index):
		"""
			Synopsis:
				Delete a char at a given <index> from [data].
			Takes:
				int i		-	Index to delete
			Returns:
				string [data]
		"""
		self.data = self.data[:index] + self.data[index+1:]
		return s(self.data)

	def split(self,char=' ',skip=0,num=-1):
		"""
			Synopsis:
				Split a string at a given <char> from [data].
			Takes:
				string char -	char to split at
				int skip	-	a control to skip several hits before splitting
				int num		-	how many times to split, 0 is all
			Defaults:
				char	-	' '(<space>)
				skip	-	0
				num		-	-1
			Returns:
				list of hits
		""" 
		buf = ""
		retval = []
		skipTog = False
		i = 0 # index for when we yank the last of the string

		for ch in self.data:
			skipTog = False
			if skip > 0:
				skip -= 1
				skipTog = True

			if num > 1: num -= 1

			if num == 0:
				# do the stuff to add the last of the string to the list
#				 retval.append(buf[i:])
				break

			if not skipTog:
				if ch == char:
					retval.append(buf)
					buf = ""
					continue
			i += 1
			buf += ch
		retval.append(buf) # append the last of the string to the list
		return retval

	def purge(self,char,num=0):
		"""
			Synopsis:
				Delete <num> instances of <char> from string [data].
			Takes:
				string char -	Char to remove
				int num		-	Number of chars to remove, Defaults
								0 to remove all.
			Returns:
				self
		"""
		if num == 0:
			# NOTE: This will always handle all instances :P
			num = len(self.data)
		for i in range(num):
			targ = self.data.find(char)
			if targ != -1:
				self.delch(targ)
			else:
				break
#		 return s(self.data)
		return self

	def rpurge(self,char,num=0):
		"""
			Synopsis:
				Helper function, see [purge].
			Takes:
				string char -	char to remove
				int num		-	number of instances to remove
			Returns:
				self
		"""
		self.reverse()
		self.purge(char,num)
		self.reverse()
		return self

	def reverse(self):
		"""
			Synopsis:
				Reverse the string.
			Takes:
				None
			Returns:
				self
		"""
		self.data = self.data[::-1]
		return self


	def trimSpaces(self):
		"""
			Synopsis:
				Trim trailing white space.
			Takes:
				None
			Returns:
				self
		"""
		# count whitespace on left side
		counter = 0
		rcounter = 0
		if self.data[0] == ' ': # we don't need to trim  left if there is no space
			for char in self.data:
				if char != ' ': break
				counter += 1
			else: counter = None # none is the same as string[:] which is the string

		self.reverse()

		if self.data[0] == ' ':
			for char in self.data:
				if char != ' ': break
				rcounter += 1
			rcounter *= -1
		else: rcounter = None

		self.reverse() # extremely important. Forgetting this results in lulz :P

		self.data = self.data[counter:rcounter]
		return self

	def __str__(self):
		return self.data
	def __repr__(self):
		return "s\"%s\"" % self.data

if __name__ == "__main__":
	from wst.trm import log
	log.logger = "-=[Test Harness]=-"
	print("")

	log.warn("Beginning...")
	log.good("Calling->s(\"test of strings\")")
	testString = s("test of strings")
	print("")

	log.log("Test of print")
	log.good("Calling->print(testString)")
	print(testString)
	print("")

	log.log("Test of split")
	log.good("Calling->print(testString.split())")
	print(testString.split())

	testString = s("     space trimming     test     ")
	log.log("Test of edge space trimming")
	log.good("Calling->print(testString.trimSpaces())")
	print("_%s_" %testString.trimSpaces())

